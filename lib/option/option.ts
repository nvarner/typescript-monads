export const OptionType = {
  Some: Symbol(":some"),
  None: Symbol(":none"),
};

export interface Match<T, U> {
  some: (val: T) => U;
  none: (() => U) | U;
}

/**
 * # Rust-inspired `Option<T>` type
 *
 * Original implementation: <https://doc.rust-lang.org/std/option/enum.Option.html>
 *
 * Type `Option<T>` represents an optional value: every `Option` is either `Some` and contains a value, or `None`, and does not.
 *
 * You could consider using `Option` for:
 *
 * -   Nullable pointers (`undefined` in JavaScript)
 * -   Return value for otherwise reporting simple errors, where None is returned on error
 * -   Default values and/or properties
 * -   Nested optional object properties
 *
 * `Option`s are commonly paired with pattern matching to query the presence of a value and take action, always accounting for the `None` case.
 *
 * ```typescript
 * function divide(numerator: number, denominator: number): Option<number> {
 *   if (denominator === 0) {
 *     return None
 *   } else {
 *     return Some(numerator / denominator)
 *   }
 * }
 *
 * // The return value of the function is an option
 * const result = divide(2.0, 3.0)
 *
 * // Pattern match to retrieve the value
 * const message = result.match({
 *   some: res => `Result: ${res}`,
 *   none: "Cannot divide by 0",
 * })
 *
 * console.log(message) // "Result: 0.6666666666666666"
 *
 * ```
 */
export interface Option<T> {
  type: symbol;

  /**
   * Returns `true` if the option is a `Some` value.
   *
   * #### Examples
   *
   * ```typescript
   * let x: Option<number> = Some(2)
   * console.log(x.isSome()) // true
   *
   * ```
   *
   * ```typescript
   * let x: Option<number> = None
   * console.log(x.isSome()) // false
   *
   * ```
   *
   * #### `null` and `undefined`
   *
   * In previous versions of this package, calling `Some(val)` where `val` is either `null` or `undefined` would result in `None`.
   *
   * Hovewer, `null` is considered a _value_ and it is internally treated as `object`.
   *
   * Therefore, as of version `v3.0.0`, constructing `Some` with `null` (explicitly, or implicitly) will indeed yield `Some<null>`.
   *
   * ```typescript
   * expect(Some().isSome()).toEqual(false)
   * expect(Some(undefined).isSome()).toEqual(false)
   *
   * // This assertion would fail in versions below 3.0.0
   * expect(Some(null).isSome()).toEqual(true)
   *
   * ```
   */
  isSome(): boolean;

  /**
   * Returns `true` if the option is a `None` value.
   *
   * #### Examples
   *
   * ```typescript
   * let x: Option<number> = Some(2)
   * console.log(x.isNone()) // false
   *
   * ```
   *
   * ```typescript
   * let x: Option<number> = None
   * console.log(x.isNone()) // true
   *
   * ```
   */
  isNone(): boolean;

  /**
   * Applies a function to retrieve a contained value if `Option` is `Some`; Either returns, or applies another function to
   * return, a fallback value if `Option` is `None`.
   *
   * #### Examples
   *
   * ```typescript
   * const getFullYear = (date: Option<Date>): number => date.match({
   *   some: val => val.getFullYear(),
   *   none: 1994
   * })
   *
   * const someDate = Some(new Date(2017, 1))
   * const noDate = None
   *
   * console.log(getFullYear(someDate)) // 2017
   * console.log(getFullYear(noDate)) // 1994
   *
   * ```
   */
  match<U>(fn: Match<T, U>): U;

  /**
   * Runs a function iff `Option` is `Some`. There is no return value because the `None` case is not handled, so the function
   * is only run for side effects.
   *
   * #### Examples
   *
   * ```typescript
   * const printFullYearIfAvailable = (date: Option<Date>): number => date.ifSome(val => console.log(val.getFullYear()))
   *
   * const someDate = Some(new Date(2017, 1))
   * const noDate = None
   *
   * printFullYearIfAvailable(someDate) // 2017
   * printFullYearIfAvailable(noDate) // nothing printed
   *
   * ```
   */
  ifSome(fn: (val: T) => void): void;

  /**
   * Maps an `Option<T>` to `Option<U>` by applying a function to a contained value.
   *
   * #### Examples
   *
   * ```typescript
   * let x: Option<string> = Some("123")
   * let y: Option<number> = x.map(parseInt)
   *
   * console.log(y.isSome()) // true
   * console.log(y.unwrap()) // 123
   *
   * ```
   *
   * ```typescript
   * let x: Option<string> = None
   * let y: Option<number> = x.map(parseInt)
   *
   * console.log(y.isNone()) // true
   *
   * ```
   */
  map<U>(fn: (val: T) => U): Option<U>;

  /**
   * Returns `None` if the option is `None`, otherwise calls `fn` with the wrapped value and returns the result.
   *
   * Some languages call this operation `flatmap`.
   *
   * #### Examples
   *
   * ```typescript
   * const sq = (x: number): Option<number> => Some(x * x)
   * const nope = (_: number): Option<number> => None
   *
   * console.log(Some(2).andThen(sq).andThen(sq)) // Some(16)
   * console.log(Some(2).andThen(sq).andThen(nope)) // None
   * console.log(Some(2).andThen(nope).andThen(sq)) // None
   * console.log(None.andThen(sq).andThen(sq)) // None
   *
   * ```
   */
  andThen<U>(fn: (val: T) => Option<U>): Option<U>;

  /**
   * Returns the option if it contains a value, otherwise returns `optb`.
   *
   * #### Examples
   *
   * ```typescript
   * let x = Some(2)
   * let y = None
   *
   * console.log(x.or(y)) // Some(2)
   *
   * ```
   *
   * ```typescript
   * let x = None
   * let y = Some(100)
   *
   * console.log(x.or(y)) // Some(100)
   *
   * ```
   *
   * ```typescript
   * let x = Some(2)
   * let y = Some(100)
   *
   * console.log(x.or(y)) // Some(2)
   *
   * ```
   *
   * ```typescript
   * let x: Option<number> = None
   * let y = None
   *
   * console.log(x.or(y)) // None
   *
   * ```
   */
  or<U>(optb: Option<U>): Option<T | U>;

  /**
   * Returns `None` if the option is `None`, otherwise returns `optb`.
   *
   * #### Examples
   *
   * ```typescript
   * let x = Some(2)
   * let y = None
   *
   * console.log(x.and(y)) // None
   *
   * ```
   *
   * ```typescript
   * let x = None
   * let y = Some(100)
   *
   * console.log(x.and(y)) // None
   *
   * ```
   *
   * ```typescript
   * let x = Some(2)
   * let y = Some(100)
   *
   * console.log(x.and(y)) // Some(100)
   *
   * ```
   *
   * ```typescript
   * let x: Option<number> = None
   * let y = None
   *
   * console.log(x.and(y)) // None
   *
   * ```
   */
  and<U>(optb: Option<U>): Option<U>;

  /**
   * Returns the contained value or `optb`.
   *
   * #### Examples
   *
   * ```typescript
   * console.log(Some("car").unwrapOr("bike")) // "car"
   * console.log(None.unwrapOr("bike")) // "bike"
   *
   * ```
   */
  unwrapOr(def: T): T;

  /**
   * Returns the contained value or the result of ``.
   *
   * #### Examples
   *
   * ```typescript
   * console.log(Some("car").unwrapOrElse(() => "bi" + "ke")) // "car"
   * console.log(None.unwrapOrElse(() => "bi" + "ke"))) // "bike"
   *
   * ```
   */
  unwrapOrElse(f: () => T): T;

  /**
   * Moves the value `v` out of the `Option<T>` if it is `Some(v)`.
   *
   * In general, because this function may throw, its use is discouraged.
   * Instead, try to use `match` and handle the `None` case explicitly.
   *
   * #### Throws
   *
   * Throws a `ReferenceError` if the option is `None`.
   *
   * #### Examples
   *
   * ```typescript
   * let x = Some("air")
   * console.log(x.unwrap()) // "air"
   *
   * ```
   *
   * ```typescript
   * let x = None
   * console.log(x.unwrap()) // fails, throws an Exception
   *
   * ```
   *
   * Alternatively, you can choose to use `isSome()` to check whether the option is `Some`. This will enable you to use `unwrap()` in the `true` / success branch.
   *
   * ```typescript
   * function getName(name: Option<string>): string {
   *   if (isSome(name)) {
   *     return name.unwrap()
   *   } else {
   *     return "N/A"
   *   }
   * }
   *
   * ```
   */
  unwrap(): T | never;
}

export interface OptSome<T> extends Option<T> {
  unwrap(): T;
  map<U>(fn: (val: T) => U): OptSome<U>;
  or<U>(optb: Option<U>): Option<T>;
  and<U>(optb: Option<U>): Option<U>;
}

export interface OptNone<T> extends Option<T> {
  unwrap(): never;
  map<U>(fn: (val: T) => U): OptNone<U>;
  or<U>(optb: Option<U>): Option<U>;
  and<U>(optb: Option<U>): OptNone<U>;
}

export function Some<T>(val?: T | undefined): Option<T> {
  return typeof val === "undefined"
    ? none_constructor<T>()
    : some_constructor<T>(val as T);
}

export const None = none_constructor<any>();

export function some_constructor<T>(val: T): OptSome<T> {
  if (typeof val === "undefined") {
    throw new TypeError(
      "Some has to contain a value. Constructor received undefined.",
    );
  }

  return {
    type: OptionType.Some,
    isSome(): boolean {
      return true;
    },
    isNone(): boolean {
      return false;
    },
    match<U>(fn: Match<T, U>): U {
      return fn.some(val);
    },
    ifSome(fn: (val: T) => void): void {
      fn(val);
    },
    map<U>(fn: (val: T) => U): OptSome<U> {
      return some_constructor<U>(fn(val));
    },
    andThen<U>(fn: (val: T) => Option<U>): Option<U> {
      return fn(val);
    },
    or<U>(_optb: Option<U>): Option<T> {
      return this;
    },
    and<U>(optb: Option<U>): Option<U> {
      return optb;
    },
    unwrapOr(_def: T): T {
      return val;
    },
    unwrapOrElse(_f: () => T): T {
      return val;
    },
    unwrap(): T {
      return val;
    },
  };
}

export function none_constructor<T>(): OptNone<T> {
  return {
    type: OptionType.None,
    isSome(): boolean {
      return false;
    },
    isNone(): boolean {
      return true;
    },
    match<U>(matchObject: Match<T, U>): U {
      const { none } = matchObject;

      if (typeof none === "function") {
        return (none as () => U)();
      }

      return none;
    },
    ifSome(_fn: (val: T) => void): void {},
    map<U>(_fn: (val: T) => U): OptNone<U> {
      return none_constructor<U>();
    },
    andThen<U>(_fn: (val: T) => Option<U>): OptNone<U> {
      return none_constructor<U>();
    },
    or<U>(optb: Option<U>): Option<U> {
      return optb;
    },
    and<U>(_optb: Option<U>): OptNone<U> {
      return none_constructor<U>();
    },
    unwrapOr(def: T): T {
      if (def == null) {
        throw new Error("Cannot call unwrapOr with a missing value.");
      }

      return def;
    },
    unwrapOrElse(f: () => T): T {
      return f();
    },
    unwrap(): never {
      throw new ReferenceError("Trying to unwrap None.");
    },
  };
}

export function isSome<T>(val: Option<T>): val is OptSome<T> {
  return val.isSome();
}

export function isNone<T>(val: Option<T>): val is OptNone<T> {
  return val.isNone();
}

/**
 * Retrieves value `V` and converts it to `Option<V>` if key leads to this value, otherwise returns `None`. It is highly
 * recommended to cast the return type to `Option<V>` explicitly, as seen in examples below.
 *
 * #### Examples
 *
 * ```typescript
 *
 * const getDriverName = (map: Map<string, string>): Option<string> => fromMap(map, "car")
 *
 * const noCarDriver = new Map()
 * console.log(getDriverName(noCarDriver)) // None
 *
 * const hasCarDriver = new Map()
 * hasCar.set("car", "John");
 * console.log(getDriverName(hasCarDriver)) // Some("John")
 *
 * ```
 */
export function fromMap<K, V>(map: Map<K, V>, key: K): Option<V> {
  const value = map.get(key);
  if (value !== undefined) {
    return Some(value);
  } else {
    return None;
  }
}
