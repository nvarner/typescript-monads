# 👻 Monads

Type safe Option, Result, and Either types; inspired by Rust. Fork of
[@hqoss/monads](https://www.npmjs.com/package/@hqoss/monads) ([source](https://github.com/hqoss/monads))

## Table of contents

-   [⏳ Install](#-install)

    -   [Node.js and the Browser](#nodejs-and-the-browser)

-   [📝 Usage](#-usage)

    -   [`Option<T>`](#optiont)
    -   [`Result<T, E>`](#resultt-e)
    -   [`Either<L, R>`](#eitherl-r)
    -   [API Docs](#api-docs)

## ⏳ Install

### Node.js and the Browser

```bash
npm install @nvarner/monads
```

**⚠️ NOTE:** The project is configured to target `ES2018` and the library uses `commonjs` module resolution.

## 📝 Usage

### `Option<T>`

```typescript
import { Option, Some, None } from "@nvarner/monads"

function divide(numerator: number, denominator: number): Option<number> {
  if (denominator === 0) {
    return None
  } else {
    return Some(numerator / denominator)
  }
};

// The return value of the function is an option
const result = divide(2.0, 3.0)

// Pattern match to retrieve the value
const message = result.match({
  some: res => `Result: ${res}`,
  none: "Cannot divide by 0",
})

console.log(message) // "Result: 0.6666666666666666"
```

### `Result<T, E>`

```typescript
import { Result, Ok, Err } from "@nvarner/monads"

function getIndex(values: string[], value: string): Result<number, string> {
  const index = values.indexOf(value)

  switch (index) {
    case -1:
      return Err("Value not found")
  default:
    return Ok(index)
  }
}

console.log(getIndex(["a", "b", "c"], "b")) // Ok(1)
console.log(getIndex(["a", "b", "c"], "z")) // Err("Value not found")
```

### `Either<L, R>`

```typescript
import { Either } from "@nvarner/monads"

function getLabel(uncertainDate: Either<Date, string>) {
  return uncertainDate.match({
    left: date => date.toLocaleDateString(),
    right: text => `<abbr title="${text}">an uncertain date</abbr>`,
  })
}
```

### API Docs

[See full API Documentation here](https://gitlab.com/nvarner/typescript-monads/-/blob/master/docs/globals.md).
