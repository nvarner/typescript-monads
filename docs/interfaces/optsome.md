[@nvarner/monads](../README.md) › [Globals](../globals.md) › [OptSome](optsome.md)

# Interface: OptSome ‹**T**›

## Type parameters

▪ **T**

## Hierarchy

* [Option](option.md)‹T›

  ↳ **OptSome**

## Index

### Properties

* [type](optsome.md#type)

### Methods

* [and](optsome.md#and)
* [andThen](optsome.md#andthen)
* [ifSome](optsome.md#ifsome)
* [isNone](optsome.md#isnone)
* [isSome](optsome.md#issome)
* [map](optsome.md#map)
* [match](optsome.md#match)
* [or](optsome.md#or)
* [unwrap](optsome.md#unwrap)
* [unwrapOr](optsome.md#unwrapor)
* [unwrapOrElse](optsome.md#unwraporelse)

## Properties

###  type

• **type**: *symbol*

*Inherited from [Option](option.md).[type](option.md#type)*

Defined in lib/option/option.ts:50

## Methods

###  and

▸ **and**‹**U**›(`optb`: [Option](option.md)‹U›): *[Option](option.md)‹U›*

*Overrides [Option](option.md).[and](option.md#and)*

Defined in lib/option/option.ts:340

**Type parameters:**

▪ **U**

**Parameters:**

Name | Type |
------ | ------ |
`optb` | [Option](option.md)‹U› |

**Returns:** *[Option](option.md)‹U›*

___

###  andThen

▸ **andThen**‹**U**›(`fn`: function): *[Option](option.md)‹U›*

*Inherited from [Option](option.md).[andThen](option.md#andthen)*

Defined in lib/option/option.ts:190

Returns `None` if the option is `None`, otherwise calls `fn` with the wrapped value and returns the result.

Some languages call this operation `flatmap`.

#### Examples

```typescript
const sq = (x: number): Option<number> => Some(x * x)
const nope = (_: number): Option<number> => None

console.log(Some(2).andThen(sq).andThen(sq)) // Some(16)
console.log(Some(2).andThen(sq).andThen(nope)) // None
console.log(Some(2).andThen(nope).andThen(sq)) // None
console.log(None.andThen(sq).andThen(sq)) // None

```

**Type parameters:**

▪ **U**

**Parameters:**

▪ **fn**: *function*

▸ (`val`: T): *[Option](option.md)‹U›*

**Parameters:**

Name | Type |
------ | ------ |
`val` | T |

**Returns:** *[Option](option.md)‹U›*

___

###  ifSome

▸ **ifSome**(`fn`: function): *void*

*Inherited from [Option](option.md).[ifSome](option.md#ifsome)*

Defined in lib/option/option.ts:146

Runs a function iff `Option` is `Some`. There is no return value because the `None` case is not handled, so the function
is only run for side effects.

#### Examples

```typescript
const printFullYearIfAvailable = (date: Option<Date>): number => date.ifSome(val => console.log(val.getFullYear()))

const someDate = Some(new Date(2017, 1))
const noDate = None

printFullYearIfAvailable(someDate) // 2017
printFullYearIfAvailable(noDate) // nothing printed

```

**Parameters:**

▪ **fn**: *function*

▸ (`val`: T): *void*

**Parameters:**

Name | Type |
------ | ------ |
`val` | T |

**Returns:** *void*

___

###  isNone

▸ **isNone**(): *boolean*

*Inherited from [Option](option.md).[isNone](option.md#isnone)*

Defined in lib/option/option.ts:105

Returns `true` if the option is a `None` value.

#### Examples

```typescript
let x: Option<number> = Some(2)
console.log(x.isNone()) // false

```

```typescript
let x: Option<number> = None
console.log(x.isNone()) // true

```

**Returns:** *boolean*

___

###  isSome

▸ **isSome**(): *boolean*

*Inherited from [Option](option.md).[isSome](option.md#issome)*

Defined in lib/option/option.ts:86

Returns `true` if the option is a `Some` value.

#### Examples

```typescript
let x: Option<number> = Some(2)
console.log(x.isSome()) // true

```

```typescript
let x: Option<number> = None
console.log(x.isSome()) // false

```

#### `null` and `undefined`

In previous versions of this package, calling `Some(val)` where `val` is either `null` or `undefined` would result in `None`.

Hovewer, `null` is considered a _value_ and it is internally treated as `object`.

Therefore, as of version `v3.0.0`, constructing `Some` with `null` (explicitly, or implicitly) will indeed yield `Some<null>`.

```typescript
expect(Some().isSome()).toEqual(false)
expect(Some(undefined).isSome()).toEqual(false)

// This assertion would fail in versions below 3.0.0
expect(Some(null).isSome()).toEqual(true)

```

**Returns:** *boolean*

___

###  map

▸ **map**‹**U**›(`fn`: function): *[OptSome](optsome.md)‹U›*

*Overrides [Option](option.md).[map](option.md#map)*

Defined in lib/option/option.ts:338

**Type parameters:**

▪ **U**

**Parameters:**

▪ **fn**: *function*

▸ (`val`: T): *U*

**Parameters:**

Name | Type |
------ | ------ |
`val` | T |

**Returns:** *[OptSome](optsome.md)‹U›*

___

###  match

▸ **match**‹**U**›(`fn`: [Match](match.md)‹T, U›): *U*

*Inherited from [Option](option.md).[match](option.md#match)*

Defined in lib/option/option.ts:127

Applies a function to retrieve a contained value if `Option` is `Some`; Either returns, or applies another function to
return, a fallback value if `Option` is `None`.

#### Examples

```typescript
const getFullYear = (date: Option<Date>): number => date.match({
  some: val => val.getFullYear(),
  none: 1994
})

const someDate = Some(new Date(2017, 1))
const noDate = None

console.log(getFullYear(someDate)) // 2017
console.log(getFullYear(noDate)) // 1994

```

**Type parameters:**

▪ **U**

**Parameters:**

Name | Type |
------ | ------ |
`fn` | [Match](match.md)‹T, U› |

**Returns:** *U*

___

###  or

▸ **or**‹**U**›(`optb`: [Option](option.md)‹U›): *[Option](option.md)‹T›*

*Overrides [Option](option.md).[or](option.md#or)*

Defined in lib/option/option.ts:339

**Type parameters:**

▪ **U**

**Parameters:**

Name | Type |
------ | ------ |
`optb` | [Option](option.md)‹U› |

**Returns:** *[Option](option.md)‹T›*

___

###  unwrap

▸ **unwrap**(): *T*

*Overrides [Option](option.md).[unwrap](option.md#unwrap)*

Defined in lib/option/option.ts:337

**Returns:** *T*

___

###  unwrapOr

▸ **unwrapOr**(`def`: T): *T*

*Inherited from [Option](option.md).[unwrapOr](option.md#unwrapor)*

Defined in lib/option/option.ts:281

Returns the contained value or `optb`.

#### Examples

```typescript
console.log(Some("car").unwrapOr("bike")) // "car"
console.log(None.unwrapOr("bike")) // "bike"

```

**Parameters:**

Name | Type |
------ | ------ |
`def` | T |

**Returns:** *T*

___

###  unwrapOrElse

▸ **unwrapOrElse**(`f`: function): *T*

*Inherited from [Option](option.md).[unwrapOrElse](option.md#unwraporelse)*

Defined in lib/option/option.ts:294

Returns the contained value or the result of ``.

#### Examples

```typescript
console.log(Some("car").unwrapOrElse(() => "bi" + "ke")) // "car"
console.log(None.unwrapOrElse(() => "bi" + "ke"))) // "bike"

```

**Parameters:**

▪ **f**: *function*

▸ (): *T*

**Returns:** *T*
