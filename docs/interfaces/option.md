[@nvarner/monads](../README.md) › [Globals](../globals.md) › [Option](option.md)

# Interface: Option ‹**T**›

# Rust-inspired `Option<T>` type

Original implementation: <https://doc.rust-lang.org/std/option/enum.Option.html>

Type `Option<T>` represents an optional value: every `Option` is either `Some` and contains a value, or `None`, and does not.

You could consider using `Option` for:

-   Nullable pointers (`undefined` in JavaScript)
-   Return value for otherwise reporting simple errors, where None is returned on error
-   Default values and/or properties
-   Nested optional object properties

`Option`s are commonly paired with pattern matching to query the presence of a value and take action, always accounting for the `None` case.

```typescript
function divide(numerator: number, denominator: number): Option<number> {
  if (denominator === 0) {
    return None
  } else {
    return Some(numerator / denominator)
  }
}

// The return value of the function is an option
const result = divide(2.0, 3.0)

// Pattern match to retrieve the value
const message = result.match({
  some: res => `Result: ${res}`,
  none: "Cannot divide by 0",
})

console.log(message) // "Result: 0.6666666666666666"

```

## Type parameters

▪ **T**

## Hierarchy

* **Option**

  ↳ [OptSome](optsome.md)

  ↳ [OptNone](optnone.md)

## Index

### Properties

* [type](option.md#type)

### Methods

* [and](option.md#and)
* [andThen](option.md#andthen)
* [ifSome](option.md#ifsome)
* [isNone](option.md#isnone)
* [isSome](option.md#issome)
* [map](option.md#map)
* [match](option.md#match)
* [or](option.md#or)
* [unwrap](option.md#unwrap)
* [unwrapOr](option.md#unwrapor)
* [unwrapOrElse](option.md#unwraporelse)

## Properties

###  type

• **type**: *symbol*

Defined in lib/option/option.ts:50

## Methods

###  and

▸ **and**‹**U**›(`optb`: [Option](option.md)‹U›): *[Option](option.md)‹U›*

Defined in lib/option/option.ts:268

Returns `None` if the option is `None`, otherwise returns `optb`.

#### Examples

```typescript
let x = Some(2)
let y = None

console.log(x.and(y)) // None

```

```typescript
let x = None
let y = Some(100)

console.log(x.and(y)) // None

```

```typescript
let x = Some(2)
let y = Some(100)

console.log(x.and(y)) // Some(100)

```

```typescript
let x: Option<number> = None
let y = None

console.log(x.and(y)) // None

```

**Type parameters:**

▪ **U**

**Parameters:**

Name | Type |
------ | ------ |
`optb` | [Option](option.md)‹U› |

**Returns:** *[Option](option.md)‹U›*

___

###  andThen

▸ **andThen**‹**U**›(`fn`: function): *[Option](option.md)‹U›*

Defined in lib/option/option.ts:190

Returns `None` if the option is `None`, otherwise calls `fn` with the wrapped value and returns the result.

Some languages call this operation `flatmap`.

#### Examples

```typescript
const sq = (x: number): Option<number> => Some(x * x)
const nope = (_: number): Option<number> => None

console.log(Some(2).andThen(sq).andThen(sq)) // Some(16)
console.log(Some(2).andThen(sq).andThen(nope)) // None
console.log(Some(2).andThen(nope).andThen(sq)) // None
console.log(None.andThen(sq).andThen(sq)) // None

```

**Type parameters:**

▪ **U**

**Parameters:**

▪ **fn**: *function*

▸ (`val`: T): *[Option](option.md)‹U›*

**Parameters:**

Name | Type |
------ | ------ |
`val` | T |

**Returns:** *[Option](option.md)‹U›*

___

###  ifSome

▸ **ifSome**(`fn`: function): *void*

Defined in lib/option/option.ts:146

Runs a function iff `Option` is `Some`. There is no return value because the `None` case is not handled, so the function
is only run for side effects.

#### Examples

```typescript
const printFullYearIfAvailable = (date: Option<Date>): number => date.ifSome(val => console.log(val.getFullYear()))

const someDate = Some(new Date(2017, 1))
const noDate = None

printFullYearIfAvailable(someDate) // 2017
printFullYearIfAvailable(noDate) // nothing printed

```

**Parameters:**

▪ **fn**: *function*

▸ (`val`: T): *void*

**Parameters:**

Name | Type |
------ | ------ |
`val` | T |

**Returns:** *void*

___

###  isNone

▸ **isNone**(): *boolean*

Defined in lib/option/option.ts:105

Returns `true` if the option is a `None` value.

#### Examples

```typescript
let x: Option<number> = Some(2)
console.log(x.isNone()) // false

```

```typescript
let x: Option<number> = None
console.log(x.isNone()) // true

```

**Returns:** *boolean*

___

###  isSome

▸ **isSome**(): *boolean*

Defined in lib/option/option.ts:86

Returns `true` if the option is a `Some` value.

#### Examples

```typescript
let x: Option<number> = Some(2)
console.log(x.isSome()) // true

```

```typescript
let x: Option<number> = None
console.log(x.isSome()) // false

```

#### `null` and `undefined`

In previous versions of this package, calling `Some(val)` where `val` is either `null` or `undefined` would result in `None`.

Hovewer, `null` is considered a _value_ and it is internally treated as `object`.

Therefore, as of version `v3.0.0`, constructing `Some` with `null` (explicitly, or implicitly) will indeed yield `Some<null>`.

```typescript
expect(Some().isSome()).toEqual(false)
expect(Some(undefined).isSome()).toEqual(false)

// This assertion would fail in versions below 3.0.0
expect(Some(null).isSome()).toEqual(true)

```

**Returns:** *boolean*

___

###  map

▸ **map**‹**U**›(`fn`: function): *[Option](option.md)‹U›*

Defined in lib/option/option.ts:170

Maps an `Option<T>` to `Option<U>` by applying a function to a contained value.

#### Examples

```typescript
let x: Option<string> = Some("123")
let y: Option<number> = x.map(parseInt)

console.log(y.isSome()) // true
console.log(y.unwrap()) // 123

```

```typescript
let x: Option<string> = None
let y: Option<number> = x.map(parseInt)

console.log(y.isNone()) // true

```

**Type parameters:**

▪ **U**

**Parameters:**

▪ **fn**: *function*

▸ (`val`: T): *U*

**Parameters:**

Name | Type |
------ | ------ |
`val` | T |

**Returns:** *[Option](option.md)‹U›*

___

###  match

▸ **match**‹**U**›(`fn`: [Match](match.md)‹T, U›): *U*

Defined in lib/option/option.ts:127

Applies a function to retrieve a contained value if `Option` is `Some`; Either returns, or applies another function to
return, a fallback value if `Option` is `None`.

#### Examples

```typescript
const getFullYear = (date: Option<Date>): number => date.match({
  some: val => val.getFullYear(),
  none: 1994
})

const someDate = Some(new Date(2017, 1))
const noDate = None

console.log(getFullYear(someDate)) // 2017
console.log(getFullYear(noDate)) // 1994

```

**Type parameters:**

▪ **U**

**Parameters:**

Name | Type |
------ | ------ |
`fn` | [Match](match.md)‹T, U› |

**Returns:** *U*

___

###  or

▸ **or**‹**U**›(`optb`: [Option](option.md)‹U›): *[Option](option.md)‹T | U›*

Defined in lib/option/option.ts:229

Returns the option if it contains a value, otherwise returns `optb`.

#### Examples

```typescript
let x = Some(2)
let y = None

console.log(x.or(y)) // Some(2)

```

```typescript
let x = None
let y = Some(100)

console.log(x.or(y)) // Some(100)

```

```typescript
let x = Some(2)
let y = Some(100)

console.log(x.or(y)) // Some(2)

```

```typescript
let x: Option<number> = None
let y = None

console.log(x.or(y)) // None

```

**Type parameters:**

▪ **U**

**Parameters:**

Name | Type |
------ | ------ |
`optb` | [Option](option.md)‹U› |

**Returns:** *[Option](option.md)‹T | U›*

___

###  unwrap

▸ **unwrap**(): *T | never*

Defined in lib/option/option.ts:333

Moves the value `v` out of the `Option<T>` if it is `Some(v)`.

In general, because this function may throw, its use is discouraged.
Instead, try to use `match` and handle the `None` case explicitly.

#### Throws

Throws a `ReferenceError` if the option is `None`.

#### Examples

```typescript
let x = Some("air")
console.log(x.unwrap()) // "air"

```

```typescript
let x = None
console.log(x.unwrap()) // fails, throws an Exception

```

Alternatively, you can choose to use `isSome()` to check whether the option is `Some`. This will enable you to use `unwrap()` in the `true` / success branch.

```typescript
function getName(name: Option<string>): string {
  if (isSome(name)) {
    return name.unwrap()
  } else {
    return "N/A"
  }
}

```

**Returns:** *T | never*

___

###  unwrapOr

▸ **unwrapOr**(`def`: T): *T*

Defined in lib/option/option.ts:281

Returns the contained value or `optb`.

#### Examples

```typescript
console.log(Some("car").unwrapOr("bike")) // "car"
console.log(None.unwrapOr("bike")) // "bike"

```

**Parameters:**

Name | Type |
------ | ------ |
`def` | T |

**Returns:** *T*

___

###  unwrapOrElse

▸ **unwrapOrElse**(`f`: function): *T*

Defined in lib/option/option.ts:294

Returns the contained value or the result of ``.

#### Examples

```typescript
console.log(Some("car").unwrapOrElse(() => "bi" + "ke")) // "car"
console.log(None.unwrapOrElse(() => "bi" + "ke"))) // "bike"

```

**Parameters:**

▪ **f**: *function*

▸ (): *T*

**Returns:** *T*
